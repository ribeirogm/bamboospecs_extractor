#!/usr/bin/env bash

sh ./scripts/pyenv-installer.sh

export HOME_DIR=`eval echo "~$different_user"`
export PATH="$HOME_DIR/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

pyenv install 2.7.12
pyenv global 2.7.12

# Upgrade pip
$HOME_DIR/.pyenv/shims/pip install --upgrade pip
$HOME_DIR/.pyenv/shims/pip install -r scripts/requirements.txt

# Running the opup report here
mkdir -p bamboo-specs-baseline

# Remember to update "username" and "password" field to your Bamboo username / password before running.
$HOME_DIR/.pyenv/shims/python ./scripts/bamboo-spec-baseline-download.py bamboo-specs-baseline

# Remove below code if you want to keep your pyenv environment for next run
echo "Cleaning up pyenv..."
rm -rf "${HOME_DIR}/.pyenv"