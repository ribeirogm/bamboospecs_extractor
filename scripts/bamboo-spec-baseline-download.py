import sys
import os.path
import mechanize
import cookielib
from lxml import html

# Adjust bamboo base_url, username and password:

base_url = "http://bamboo:8085"
login_url = base_url + "/userlogin!doDefault.action?os_destination=%2Fstart.action"
username = "root"
password = "root"

# Browser
browser = mechanize.Browser()

# Cookie Jar
cookie_jar = cookielib.LWPCookieJar()
browser.set_cookiejar(cookie_jar)

# Browser options
browser.set_handle_equiv(True)
browser.set_handle_redirect(True)
browser.set_handle_referer(True)
browser.set_handle_robots(False)
browser.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

browser.addheaders = [('User-agent', 'Chrome')]

# The site we will navigate into, handling it's session
browser.open(login_url)

# Select the second (index one) form (the first form is a search query box)
browser.select_form(nr=0)

# User credentials
browser.form['os_username'] = username
browser.form['os_password'] = password

# Login
browser.submit()

bamboo_spec_saving_path = os.path.realpath(sys.argv[1])
with open(os.path.realpath('bamboo-spec-urls.txt'), "rb") as fr:
    for url in fr:
        print "Exporting bamboo spec for {url}".format(url=url)
        export_spec_page = browser.open(url).read()
        export_spec_page_dom_tree = html.fromstring(export_spec_page)
        bamboo_spec_elements = export_spec_page_dom_tree.xpath("//textarea[@id='exportItem_value']")

        if len(bamboo_spec_elements) > 0:
            filename = url.replace("http://bamboo:8085/exportSpecs/plan.action?buildKey=", "").strip()
            file_path = bamboo_spec_saving_path + "/" + filename + ".java"
            bamboo_spec_content = bamboo_spec_elements[0].text

            # Try to read the existing file to see if the new bamboo spec is different with it
            if os.path.isfile(file_path):
                with open(file_path, 'r') as existing_bamboo_spec_fr:
                    existing_bamboo_spec_content = existing_bamboo_spec_fr.read()
                    if bamboo_spec_content != existing_bamboo_spec_content:
                        with open(file_path, 'w') as bamboo_spec_fw:
                            bamboo_spec_fw.write(bamboo_spec_content)
            else:
                with open(file_path, 'w') as bamboo_spec_fw:
                    bamboo_spec_fw.write(bamboo_spec_content)
        else:
            print "Export failed for this url: " + url
