# BambooSpecs Extractor

Script to export the JAVA specs code for multiple plans from Bamboo UI

# Usage

Adjust `base_url`, `username`, `password` and the `url.replace` string at the `/scripts/bamboo-spec-baseline-download.py` file.

Add the URLs for the plans you want to export to the `/bamboo-spec-urls.txt` file following the example.

Run the `/resync-bamboo-spec-baseline.sh` script.
